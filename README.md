Source code for the [2nd International Conference on "Transformation of Infrastructure Systems" website](https://kritis.gitlab.io/intconf2021/) taking place on 4-5th of November, 2021, hosted by the [Research Training Group KRITIS](https://www.kritis.tu-darmstadt.de/).

Web design informed by current best practices for [Resilient Web Design](https://resilientwebdesign.com/). See also the [Lighthouse performance report](https://kritis.gitlab.io/tticonf2021/lighthouse/kritis.gitlab.io-20210427T164832.html) for this website.

Web page styling based on the pretty rad [Bulma CSS framework](https://www.bulma.io/).

Serverless web form functionality provided by [BASIN](https://www.usebasin.com/).
