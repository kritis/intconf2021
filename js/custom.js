$(document).ready(function() {
  $('#tabs li').on('click', function() {
  	// get current tab item
    var tab = $(this).data('tab');

    // remove 'is-active' class from all li-elements in div with id=tabs
    $('#tabs li').removeClass('is-active');
    // add 'is-active' class to selected li-element in current tab item
    $(this).addClass('is-active');

    // remove 'is-active' class from all div elements in div with id=tab-content
    $('#tab-content div').removeClass('is-active');
    // add 'is-active' class to div with attribute 'data-content'=tab
    $('div[data-content="' + tab + '"]').addClass('is-active');
    // add 'is-active' class to children of div with attribute 'data-content'=tab
    $('div[data-content="' + tab + '"] div').addClass('is-active');

  });
});
